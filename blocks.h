#ifndef BLOCKS
#define BLOCKS

enum Block : unsigned char
{
    EndOfStream,
    EmptySpace,
    Wall,
    Digit0,
    Digit1,
    Digit2,
    Digit3,
    Digit4,
    Digit5,
    Digit6,
    Digit7,
    Digit8,
    Digit9,
    Black,
    Gray,
    LightGray,
    White,
    Red,
    Orange,
    Yellow,
    Green,
    Blue,
    Violet,
    LightRed,
    LightOrange,
    LightYellow,
    LightGreen,
    LightBlue,
    LightViolet,
    Cyan,
    Purple,
    Magenta


};

#endif
