
#include "tetris.h"

#include <algorithm>

Tetris::Tetris()
{

}

void Tetris::init(int width, int height)
{
    mWidth = width;
    mHeight = height;
    mField.resize(width * height);
    reset();
}

void Tetris::reset()
{
    std::fill(mField.begin(), mField.end(), EmptySpace);
    mStones.clear();
    mModified = true;
    mCurrent.index = mCurrent.x = mCurrent.y = mCurrent.rotation = 0;
    mGameOver = false;
    mScore = 0;
    addScore(0);
}

void Tetris::tick(unsigned /*ms*/)
{
    if (mGameOver) return;
    if (mModified)
    {
        mModified = false;
        mRand.reset(new std::uniform_int_distribution<int>(0, static_cast<int>(mStones.size() - 1)));
        mNextStoneIndex = static_cast<int>((*mRand.get())(mRandEngine));
        mCurrent.index = static_cast<int>((*mRand.get())(mRandEngine));
        copyStoneRotated();
    }
    addScore(1);
    if (!moveStone(mCurrent.x, mCurrent.y + 1, false))
    {
        moveStone(mCurrent.x, mCurrent.y, true);
        checkLines();
        mCurrent.index = mNextStoneIndex;
        mNextStoneIndex = static_cast<int>((*mRand.get())(mRandEngine));
        mCurrent.x = (mWidth - mStones[mCurrent.index].second) / 2;
        mCurrent.y = 0;
        mCurrent.rotation = 0;
        copyStoneRotated();
        if (!moveStone(mCurrent.x, mCurrent.y, false))
        {
            mGameOver = true;
        }
    }
}

void Tetris::moveLeft()
{
    moveStone(mCurrent.x - 1, mCurrent.y, false);
}

void Tetris::moveRight()
{
    moveStone(mCurrent.x + 1, mCurrent.y, false);
}

void Tetris::rotate()
{
    ++mCurrent.rotation;
    if (mCurrent.rotation >= 4) mCurrent.rotation = 0;
    copyStoneRotated();
    if (!moveStone(mCurrent.x, mCurrent.y, false))
    {
        --mCurrent.rotation;
        if (mCurrent.rotation < 0) mCurrent.rotation = 3;
        copyStoneRotated();
    }
}

void Tetris::drop()
{
    while (moveStone(mCurrent.x, mCurrent.y + 1, false))
    { }
}

Block Tetris::block(int x, int y) const
{
    if (x <= 0 || x >= mWidth + 1) return Wall;
    if (y <= 0 || y >= mHeight + HEADER_HEIGHT + 2 || y == HEADER_HEIGHT + 1) return Wall;
    if (y <= HEADER_HEIGHT)
    {
        if (y == 1)
        {
            x -= std::max(static_cast<int>(mWidth - sizeof(mScoreAsString) + 2), 1);
            if (x < 0) return EmptySpace;
            return static_cast<Block>(mScoreAsString[x] - '0' + Digit0);
        }
        x -= 2;
        y -= 2;
        const std::vector<Block>& stone = mStones[mNextStoneIndex].first;
        int sw = mStones[mNextStoneIndex].second;
        if (x < sw && x >= 0)
        {
            int ss = stone.size();
            int sh = (ss + sw - 1) / sw;
            int index = x + y * sw;
            if (index < ss && y < sh)
            {
                return stone[index];
            }
        }
        return EmptySpace;
    }
    --x;
    y -= HEADER_HEIGHT + 2;


    if (y >= mCurrent.y && y < mCurrent.y + mCurrent.height && x >= mCurrent.x && x < mCurrent.x + mCurrent.width)
    {
        int index = x - mCurrent.x + (y - mCurrent.y) * mCurrent.width;
        Block b = mCurrent.stone[index];
        if (b != EmptySpace) return b;
    }
    return mField[x + y * mWidth];
}

bool Tetris::moveStone(int destinationX, int destinationY, bool apply)
{
    if (destinationY + mCurrent.height > mHeight) return false;
    if (destinationX < 0) return false;
    if (destinationX + mCurrent.width > mWidth) return false;
    for (int y = 0; y < mCurrent.height; ++y)
    {
        for (int x = 0; x < mCurrent.width; ++x)
        {
            int index = x + y * mCurrent.width;
            Block b = mCurrent.stone[index];
            if (b != EmptySpace)
            {
                if (mField[destinationX + x + (destinationY + y) * mWidth] != EmptySpace) return false;
                else if (apply) mField[destinationX + x + (destinationY + y) * mWidth] = b;
            }
        }
    }
    mCurrent.x = destinationX;
    mCurrent.y = destinationY;
    return true;
}

void Tetris::copyStoneRotated()
{
    const std::vector<Block>& stone = mStones[mCurrent.index].first;
    int sw = mStones[mCurrent.index].second;
    int ss = stone.size();
    int sh = (ss + sw - 1) / sw;
    if ((mCurrent.rotation % 2) == 0)
    {
        mCurrent.width = sw;
        mCurrent.height = sh;
    }
    else
    {
        mCurrent.width = sh;
        mCurrent.height = sw;
    }
    mCurrent.stone.resize(sw*sh);
    for (int y = 0; y < sh; ++y)
    {
        for (int x = 0; x < sw; ++x)
        {
            int index = x + y * sw;
            int cx = x;
            int cy = y;
            switch (mCurrent.rotation)
            {
            case 0: break;
            case 1: cx = sh - y - 1; cy = x; break;
            case 2: cx = sw - x - 1; cy = sh - y - 1; break;
            case 3: cx = y; cy = sw - x - 1; break;
            }
            Block b = EmptySpace;
            if (index < ss) b = stone[x + y * sw];
            mCurrent.stone[cx + cy * mCurrent.width] = b;
        }
    }
}

void Tetris::checkLines()
{
    int lineCount = 0;
    for (int y = 0; y < mHeight; ++y)
    {
        bool line = true;
        for (int x = 0; x < mWidth; ++x)
        {
            if (mField[x + y * mWidth] == EmptySpace)
            {
                line = false;
                break;
            }
        }
        if (line)
        {
            ++lineCount;
            for (int ly = y; ly > 0; --ly)
            {
                for (int x = 0; x < mWidth; ++x) mField[x + ly * mWidth] = mField[x + (ly - 1) * mWidth];
            }
            for (int x = 0; x < mWidth; ++x) mField[x] = EmptySpace;
        }
    }
    if (lineCount > 0) addScore((1 << (lineCount - 1)) * 100);
}

void Tetris::addScore(int amount)
{
    mScore += amount;
    int maxLen = std::min(static_cast<int>(sizeof(mScoreAsString) - 1), mWidth);
    sprintf(mScoreAsString, "%*.*i", maxLen, maxLen, mScore);
}

void Tetris::addStone(std::initializer_list<Block> description, int width)
{
    mStones.push_back(std::pair<std::vector<Block>, unsigned>(std::vector<Block>(description), width));
    mModified = true;
}

void Tetris::addDefaultStones()
{
    addStone({Cyan, Cyan, Cyan, Cyan}, 4);
    addStone({Blue, EmptySpace, EmptySpace,
              Blue, Blue, Blue}, 3);
    addStone({EmptySpace, EmptySpace, Orange,
              Orange, Orange, Orange}, 3);
    addStone({Yellow, Yellow,
              Yellow, Yellow}, 2);
    addStone({EmptySpace, Green, Green,
              Green, Green}, 3);
    addStone({Red, Red, EmptySpace,
              EmptySpace, Red, Red}, 3);
    addStone({EmptySpace, Purple, EmptySpace,
              Purple, Purple, Purple}, 3);
}
