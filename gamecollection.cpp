#include "gamecollection.h"
#include "tetris.h"

#include <stdio.h>
#include <termios.h>
#include <unistd.h>

#include <thread>
#include <iostream>
#include <chrono>

GameCollection::GameCollection()
{
}

void setColor(Block b)
{
    int c = 40;
    switch (b)
    {
    case EmptySpace:
    case Black: c = 40; break;
    case Gray:  c = 40; break;
    case LightGray:
    case Digit0:
    case Digit1:
    case Digit2:
    case Digit3:
    case Digit4:
    case Digit5:
    case Digit6:
    case Digit7:
    case Digit8:
    case Digit9:
    case White: c = 47; break;
    case Red:   c = 41; break;
    case Orange:
    case Yellow:c = 43; break;
    case Green: c = 42; break;
    case Blue:  c = 44; break;
    case Violet:
    case LightRed:
    case LightOrange:
    case LightYellow:
    case LightGreen:
    case LightBlue:
    case LightViolet:
    case Cyan:  c = 46; break;
    case Purple:
    case Wall:
    case Magenta:c = 45; break;
    }
    printf("\x1b[0;30;%im", c);
}

void runTetris(Tetris* pt)
{
    Tetris& tetris = *pt;
    tetris.init(20, 20);
    tetris.addDefaultStones();

    while (!tetris.gameOver())
    {
        tetris.tick(200);
        int width = tetris.screenWidth();
        int height = tetris.screenHeight();
        setColor(EmptySpace);
        printf("\x1b[2J");
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                Block b = tetris.block(x, y);
                setColor(b);
                if (b >= Digit0 && b <= Digit9) printf(" %c", '0' + b - Digit0);
                else printf("  ");
            }
            setColor(EmptySpace);
            printf("\n");
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }

}

char getch()
{
    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
        perror ("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        perror ("tcsetattr ~ICANON");
    return (buf);
}

int main()
{
    Tetris tetris;

    std::thread tt(runTetris, &tetris);
    char c;
    while ((c = getch()) > 0)
    {
        if (c == 'a') tetris.moveLeft();
        else if (c == 'd') tetris.moveRight();
        else if (c == 'w') tetris.rotate();
        else if (c == 's') tetris.drop();
    }
}
