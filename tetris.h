#ifndef TETRIS_H
#define TETRIS_H

#include "blocks.h"

#include <vector>
#include <string>
#include <random>
#include <memory>
#include <initializer_list>

class Tetris
{
public:
    static const int HEADER_HEIGHT = 4;
    static const int NEXT_STONE_WIDTH = HEADER_HEIGHT * 2;
    Tetris();

    void init(int width, int height);
    void reset();
    void addStone(std::initializer_list<Block> description, int width);
    void addDefaultStones();
    void tick(unsigned ms);
    void moveLeft();
    void moveRight();
    void rotate();
    void drop();

    int screenWidth() const { return mWidth + 2; }
    int screenHeight() const { return mHeight + HEADER_HEIGHT + 3; }
    Block block(int x, int y) const;
    bool gameOver() const { return mGameOver; }

private:
    bool moveStone(int destinationX, int destinationY, bool apply);
    void copyStoneRotated();
    void checkLines();
    void addScore(int amount);

private:
    struct StoneState
    {
        std::vector<Block> stone;
        int width;
        int height;
        int index;
        int x;
        int y;
        int rotation;
    };
    int mWidth;
    int mHeight;
    std::vector<Block> mField;
    std::vector<std::pair<std::vector<Block>, int>> mStones;
    int mScore;
    char mScoreAsString[10];
    bool mModified = true;
    bool mGameOver = false;
    int mNextStoneIndex = -1;
    StoneState mCurrent;
    std::default_random_engine mRandEngine;
    std::unique_ptr<std::uniform_int_distribution<int>> mRand;
};

#endif // TETRIS_H
